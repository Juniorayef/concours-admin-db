<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Pré-réquis

Cette application d'inscription à un concours de jeux vidéos utilise le Framework Laravel qui est basé sur PHP

- Avoir une connexion internet
- [Avoir téléchargé et installé NodeJs dont voici le lien](https://nodejs.org/en/download/).
- [Avoir téléchargé et installé Composer dont voici le lien](https://nodejs.org/en/download/).
- [Avoir téléchargé et installé Git dont voici le lien](https://git-scm.com/downloads).
- Avoir PHP 8.0.13 ou version supérieur
- MySQL 8.0.27 ou version supérieur


## Installation

1. Cloner le repo dans votre repertoire www de wampserver ou htdocs de xamserver à travers la commande:
    `git clone https://gitlab.com/Juniorayef/concours-admin-db.git`

2. Se placer dans le répertoire contenant le projet et exécuter les commandes suivantes:
    - `composer install`
    - `composer update`
    - `npm install`
    - `npm run dev`

3. Dans le répertoire racine du projet, renommer le fichier `.env.example` en `.env`

4. Dans ce fichier .env, remplir les champs DB_DATABASE, DB_USERNAME, DB_PASSWORD correspondant repectivement au nom de la base de données que vous avez créé, le nom d'utilisateur et le mot de passe de cette base de données.

4. Exécuter les commandes suivantes: 
    - `php artisan key:generate`
    - `php artisan migrate:refresh`
    - `php artisan migrate:fresh --seed`

## Exécution
 - Taper la commande `php artisan server` et laisser tourner pour héberger le site.
 - Accéder au site à travers le liens [127.0.0.1:8000](http://127.0.0.1:8000) 
 - L'adresse email d'administration est `ayefoujunior@gmail.com` et le mot de passe `password`
