<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInscriptionRequest;
use App\Http\Requests\UpdateInscriptionRequest;
use App\Models\Domaine;
use App\Models\Inscription;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

class InscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $role = User::find(Auth::user()->id)->roles[0]->title;
        $user = User::find(Auth::user()->id);
        $inscriptions = $user->inscriptions;
        if ($role == 'Admin') {
            $inscriptions = Inscription::all();
        }


        return view('inscriptions.index', compact('inscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = User::find(Auth::user()->id)->roles[0]->title;
        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $domaines = Domaine::pluck('title', 'id');
        $users = User::pluck('name', 'id');
        return view('inscriptions.create', compact('domaines', 'role', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInscriptionRequest $request)
    {
        try {
            DB::beginTransaction();
            $role = User::find(Auth::user()->id)->roles[0]->title;

            $inscription = Inscription::create($request->validated());

            if ($role != 'Admin') {

                $inscription->status = 2;

                $inscription->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            echo 'Message: ' . $e->getMessage();
        }




        //DB::rollBack();

        return redirect()->route('inscriptions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Inscription $inscription)
    {
        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('inscriptions.show', compact('inscription'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inscription $inscription)
    {
        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $domaines = Domaine::pluck('title', 'id');

        $role = User::find(Auth::user()->id)->roles[0]->title;

        $users = User::pluck('name', 'id');

        return view('inscriptions.edit', compact('inscription', 'domaines', 'role', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInscriptionRequest $request, Inscription $inscription)
    {
        // DB::transaction(function () {
        //     DB::update('update inscriptions set votes = 1');
        //     // DB::delete('delete from posts');
        //     DB::table('inscriptions')->where('id', $inscription->id)


        // }, 5);
        DB::transaction(function () use ($inscription, $request) {
            $inscription->update($request->validated());
        }, 5);


        return redirect()->route('inscriptions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inscription $inscription)
    {
        abort_if(Gate::denies('task_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $inscription->delete();

        return redirect()->route('inscriptions.index');
    }
}
