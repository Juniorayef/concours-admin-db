<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'domaine_id',
        'status',
        'motivation'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function domaine(){
        return $this->belongsTo(Domaine::class);
    }
}
