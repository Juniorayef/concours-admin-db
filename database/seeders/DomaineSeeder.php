<?php

namespace Database\Seeders;

use App\Models\Domaine;
use Illuminate\Database\Seeder;

class DomaineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domaines = [
            [
                'id'             => 1,
                'title'           => 'FIFA 22',
            ],
            [
                'id'             => 2,
                'title'           => 'PES 2022',
            ],
            [
                'id'             => 3,
                'title'           => 'Assassin\'s creed odyssey',
            ],
            [
                'id'             => 4,
                'title'           => 'God of War: Ragnarök',
            ],
            [
                'id'             => 5,
                'title'           => 'Elden Ring',
            ],
            [
                'id'             => 6,
                'title'           => 'WWE 2K22',
            ],
            [
                'id'             => 7,
                'title'           => 'Moto GP 22',
            ],
            [
                'id'             => 8,
                'title'           => 'Hitman 3',
            ],
            [
                'id'             => 9,
                'title'           => 'NBA 2K22 2k',
            ],
            [
                'id'             => 10,
                'title'           => 'Devil may cry 5',
            ],
        ];

        Domaine::insert($domaines);
    }
}
