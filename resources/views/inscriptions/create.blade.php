<x-app-layout>
    <x-slot name="header">
        <div class="flex-row justify-content-between" style="display: flex; justify-content: space-between;" >
            <div class="col-md-6">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    S'inscrire
                </h2>

            </div>
            <div class="col-md-6">
                <a href="{{ route('inscriptions.index') }}" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Mes inscriptions
                </a>
            </div>
        </div>
    </x-slot>

    <div>
        <div class="max-w-4xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="mt-5 md:mt-0 md:col-span-2">
                <form method="post" action="{{ route('inscriptions.store') }}">
                    @csrf
                    <div class="shadow overflow-hidden sm:rounded-md">
                        @if ( $role == 'Admin')
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="users" class="block font-medium text-sm text-gray-700">Lauréat <span class="text-red-800">*</span></label>
                            <select name="user_id" id="users" class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                <option value=""></option>
                                @foreach($users as $id => $user)
                                    <option value="{{ $id }}">{{ $user }}</option>
                                @endforeach
                            </select>
                            @error('user_id')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        @else
                            <input type="text" name="user_id" value="{{Auth::user()->id}}" hidden>
                        @endif
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="domaines" class="block font-medium text-sm text-gray-700">Domaine <span class="text-red-800">*</span></label>
                            <select name="domaine_id" id="domaines" class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                <option value=""></option>
                                @foreach($domaines as $id => $domaine)
                                    <option value="{{ $id }}"{{ in_array($id, old('domaines', [])) ? ' selected' : '' }}>{{ $domaine }}</option>
                                @endforeach
                            </select>
                            @error('domaine_id')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="motivation" class="block font-medium text-sm text-gray-700">Votre motivation</label>
                            <textarea name="motivation" id="motivation" cols="30" rows="6" class="form-input rounded-md shadow-sm mt-1 block w-full">
                                {{ old('motivation', '') }}
                            </textarea>
                            @error('motivation')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        @if ( $role == 'Admin')
                            <div class="px-4 py-5 bg-white sm:p-6">
                                <label for="status" class="block font-medium text-sm text-gray-700">Statut</label>
                                <select name="status" id="status" class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                    <option value="1">Approuvé</option>
                                    <option value="2">En attente</option>
                                    <option value="0">Rejeté</option>
                                </select>
                                @error('status')
                                    <p class="text-sm text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                        @endif



                        <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                S'inscrire maintenant
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
