<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Modifier
        </h2>
    </x-slot>

    <div>
        <div class="max-w-4xl mx-auto py-10 sm:px-6 lg:px-8">
            <div class="mt-5 md:mt-0 md:col-span-2">
                <form method="post" action="{{ route('inscriptions.update', $inscription->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="shadow overflow-hidden sm:rounded-md">
                        @if ($role == 'Admin')
                            <div class="px-4 py-5 bg-white sm:p-6">
                                <label for="users" class="block font-medium text-sm text-gray-700">Lauréat <span
                                        class="text-red-800">*</span></label>
                                <select name="user_id" id="users"
                                    class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                    @foreach ($users as $id => $user)
                                        <option value="{{ $id }}" {{ $id == $inscription->user->id ? 'selected' : '' }}>{{ $user }}</option>
                                    @endforeach
                                </select>
                                @error('user_id')
                                    <p class="text-sm text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                        @endif
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="domaines" class="block font-medium text-sm text-gray-700">Domaine</label>
                            <select name="domaine_id" id="domaines"
                                class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                @foreach ($domaines as $id => $domaine)
                                    <option value="{{ $id }}"
                                        {{ $id == $inscription->domaine->id ? ' selected' : '' }}>
                                        {{ $domaine }}</option>
                                @endforeach
                            </select>
                            @error('domaine_id')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <label for="motivation" class="block font-medium text-sm text-gray-700">Motivation</label>
                            <input type="text" name="motivation" id="motivation" type="text"
                                class="form-input rounded-md shadow-sm mt-1 block w-full"
                                value="{{ old('motivation', $inscription->motivation) }}" />
                            @error('motivation')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>

                        @if ( $role == 'Admin')
                            <div class="px-4 py-5 bg-white sm:p-6">
                                <label for="status" class="block font-medium text-sm text-gray-700">Statut</label>
                                <select name="status" id="status" class="form-multiselect block rounded-md shadow-sm mt-1 block w-full">
                                    <option {{$inscription->status == 1 ? 'selected' : ''}} value="1">Approuvé</option>
                                    <option {{$inscription->status == 2 ? 'selected' : ''}} value="2">En attente</option>
                                    <option {{$inscription->status == 0 ? 'selected' : ''}} value="0">Rejeté</option>
                                </select>
                                @error('status')
                                    <p class="text-sm text-red-600">{{ $message }}</p>
                                @enderror
                            </div>
                        @endif



                        <div class="flex items-center justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button
                                class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                Mettre à jour
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
