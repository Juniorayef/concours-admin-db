<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="landingpage1.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>
        Concours de jeux vidéos
    </title>
    <link rel="stylesheet" href="{{ asset('css/homestyle.css') }}">
    <link rel="stylesheet" href="{{ asset('css/homestyle2.css') }}">
</head>

<body>
    <!--Main Start-->
    <div class="main">
        <!--Navbar Start-->
        <nav class="navbar  navbar-expand-lg navbar-dark bg-transparent" style="font-family: 'Luckiest Guy', cursive;">
            <a class="navbar-brand" href="#" style="padding-left: 150px; font-size: 40px; ">Concours de jeux vidéos</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav" style="margin-left: 35%; font-size: 20px;">
                    @guest
                        <a class="nav-item nav-link active" href="{{ route('login') }}" id="navicon">Se connecter <span
                                class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="{{ route('register') }}" id="navicon">S'inscrire</a>
                    @endguest
                    @auth
                        <a class="nav-item nav-link active" href="{{ route('dashboard') }}" id="navicon">Dashboard <span
                                class="sr-only">(current)</span></a>
                    @endauth
                </div>
            </div>
        </nav>
        <!--Navbar End-->
        <div class="lefttext">
            <div class="textwrap">
                <h1>
                    Lionel Messi
                </h1>
                <p>
                    Lionel Andrés Messi (né le 24 juin 1987), également connu sous le nom de Leo Messi, est un
                    footballeur professionnel argentin qui est capitaine de l'équipe nationale argentine et joue pour le
                    club de Ligue 1 Paris Saint-Germain.
                </p>
                <button onclick="window.location='/inscriptions/create' ">Participer au concours</button>
            </div>

        </div>
        <div class="rightimage">
            <img src="https://www.ixpaper.com/wp-content/uploads/2021/06/lionel-messi-hd-wallpaper-ixpaper.jpg">
        </div>
    </div>
    <div id="root">
        <div>
            <div>

            </div>

            <div id="menu-bar">
                <div class="menu-heading"><img
                        src="https://media.contentapi.ea.com/content/dam/eacom/common/ea-wordmark-network-nav-coral.svg"
                        alt="Electronics Arts Home"><i class="bi bi-x"></i></div>
                <ul>
                    <li><a href="#">Featured Games</a></li>
                    <li><a href="#">Shop on Origin</a></li>
                    <li><a href="#">EA Play</a></li>
                    <li><a href="#">Positive Play</a></li>
                    <li><a href="#">Company</a></li>
                    <li><a href="#">EA Studios</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Competitive Gaming</a></li>
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Investors</a></li>
                    <li><a href="#">Press</a></li>
                </ul>
            </div>
            <div id="side-nav">
                <div class="heading"><a href="#">
                        <p>All Games</p>
                    </a><i class="bi bi-x"></i></div>
                <div class="ea-play"><img class="background-img"
                        src="https://media.contentapi.ea.com/content/dam/gin/images/2021/04/ea-play-16x9-png.adapt.crop1x1.767w."
                        alt="EA Play logo background">
                    <div><img id="ea-logo"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2021/04/ea-play-mono-logo-svg.svg"
                            alt="EA Play"></div>
                </div>
                <ul>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2021/06/battlefield-2042-key-art.jpg.adapt.crop1x1.767p.jpg"
                            alt="Battlefield 2042 Poster">
                        <div class="game-logo"><img class="game-image battlefield-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/battlefield-2042-mono-logo-svg.svg"
                                alt="Battlefield 2042 Logo"></div>
                    </li>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2021/07/fifa22-key-art.jpg.adapt.crop1x1.767p.jpg"
                            alt="FIFA 22 Poster">
                        <div class="game-logo"><img class="game-image fifa-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/fifa22-mono-logo.svg"
                                alt="FIFA 22 logo"></div>
                    </li>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2021/04/f1-2021-keyart.jpg.adapt.crop1x1.767p.jpg"
                            alt="F1 2021 Poster">
                        <div class="game-logo"><img class="game-image f1-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/f1-2021-mono-logo.svg"
                                alt="F1 21 logo"></div>
                    </li>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2020/10/nfshpr-keyart-1x1.jpg.adapt.crop1x1.767p.jpg"
                            alt="Need For Speed Hot Pursuit Poster">
                        <div class="game-logo"><img class="game-image nfs-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/nfshpr-logo-white.png"
                                alt="Need For Speed Hot Pursuit logo"></div>
                    </li>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2019/01/apex-legends-keyart.jpg.adapt.crop1x1.767p.jpg"
                            alt="Apex Legends Poster">
                        <div class="game-logo"><img class="game-image apex-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/apex-legends-mono-logo.svg"
                                alt="Apex Legends logo"></div>
                    </li>
                    <li><img class="game"
                            src="https://media.contentapi.ea.com/content/dam/gin/images/2017/01/the-sims-4-keyart.jpg.adapt.crop1x1.767p.jpg"
                            alt="Sims 4 Poster">
                        <div class="game-logo"><img class="game-image sims-game"
                                src="https://media.contentapi.ea.com/content/dam/gin/common/logos/the-sims-4-mono-logo.png"
                                alt="Sims 4 logo"></div>
                    </li>
                </ul>
            </div>
            <main>
                <section class="main-poster">
                    <div class="fifa-22-info"><img
                            src="https://media.contentapi.ea.com/content/dam/eacom/images/2021/07/f22-logo-announce-7x2-xl.png.adapt.crop5x2.1455w.png">
                        <h1><strong>Powered By Football</strong></h1>
                        <p>A new season of innovation across every mode in the game.</p>
                        <div><button><a href="#">Available Now</a></button></div>
                    </div>
                    <div class="fifa-22-poster"><img
                            src="https://i.gadgets360cdn.com/large/FIFA_22_announcement_1626074539139.jpg?downsize=950:*"
                            alt="FIFA 22 Poster"></div>
                </section>
                <section class="featured-games">
                    <h2>Featured Games</h2>
                    <ul>
                        <li class="game-info"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2021/06/battlefield-2042-key-art.jpg.adapt.crop1x1.767p.jpg"
                                alt="Battlefield 2042 Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/battlefield-2042-mono-logo-svg.svg"
                                    alt="Battlefield 2042 Logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                        <li class="game-info"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2021/07/fifa22-key-art.jpg.adapt.crop1x1.767p.jpg"
                                alt="FIFA 22 Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/fifa22-mono-logo.svg"
                                    alt="FIFA 22 logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                        <li class="game-info"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2021/04/f1-2021-keyart.jpg.adapt.crop1x1.767p.jpg"
                                alt="F1 2021 Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/f1-2021-mono-logo.svg"
                                    alt="F1 21 logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                        <li class="game-info"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2020/10/nfshpr-keyart-1x1.jpg.adapt.crop1x1.767p.jpg"
                                alt="Need For Speed Hot Pursuit Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/nfshpr-logo-white.png"
                                    alt="Need For Speed Hot Pursuit logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                        <li class="game-info apex"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2019/01/apex-legends-keyart.jpg.adapt.crop1x1.767p.jpg"
                                alt="Apex Legends Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/apex-legends-mono-logo.svg"
                                    alt="Apex Legends logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                        <li class="game-info"><img
                                src="https://media.contentapi.ea.com/content/dam/gin/images/2017/01/the-sims-4-keyart.jpg.adapt.crop1x1.767p.jpg"
                                alt="Sims 4 Poster">
                            <div><img
                                    src="https://media.contentapi.ea.com/content/dam/gin/common/logos/the-sims-4-mono-logo.png"
                                    alt="Sims 4 logo">
                                <p>Official Site</p>
                            </div>
                        </li>
                    </ul>
                    <div class="latest-games"><button><a href="#">Latest Games</a></button></div>
                </section>
                <section class="latest-updates">
                    <h2>Latest Updates</h2>
                    <ul class="categories">
                        <li><a href="#">EA News</a></li>
                        <li><a href="#">EA Play</a></li>
                        <li><a href="#">FIFA</a></li>
                        <li><a href="#">Apex Legends</a></li>
                        <li><a href="#">Battlefield</a></li>
                        <li><a href="#">The Sims 4</a></li>
                        <li><a href="#">F1 2021</a></li>
                        <li><a href="#">Inside EA</a></li>
                    </ul>
                    <section class="articles">
                        <ul>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/eacom/common/ea-tile-origin-experience.png.adapt.crop16x9.431p.png"
                                        alt="EA article photo">
                                    <section>
                                        <div><span><strong>Electronic Arts
                                                    Inc.</strong></span><strong>07-Oct-2021</strong></div>
                                        <h2>EA Sports Comments on the Future of Football</h2>
                                        <p>A message to the global football community from Cam Weber, EA SPORTS Group
                                            General Manager.</p>
                                    </section>
                                </article>
                            </li>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/eacom/common/ea-tile-origin-experience.png.adapt.crop16x9.431p.png"
                                        alt="EA Poster">
                                    <section>
                                        <div><span><strong>Electronic Arts
                                                    Inc.</strong></span><strong>30-Sep-2021</strong></div>
                                        <h2>Battlefield Briefing - Exploring Battlefield™ Portal</h2>
                                        <p>Key changes in our Executive Leadership Team</p>
                                    </section>
                                </article>
                            </li>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/eacom/lost-in-random/images/2021/06/lost-in-random-feature-image-16x9.jpg.adapt.crop16x9.431p.jpg"
                                        alt="Lost in Random Poster">
                                    <section>
                                        <div><span><strong>Lost In Random</strong></span><strong>10-Sep-2021</strong>
                                        </div>
                                        <h2>A THANK YOU LETTER FROM THE LOST IN RANDOM TEAM</h2>
                                        <p>Welcome to the world of Random!</p>
                                    </section>
                                </article>
                            </li>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/eacom/images/2020/06/eappp-featured-image-logo-shapes.png.adapt.crop16x9.431p.png"
                                        alt="Positive Play Logo">
                                    <section>
                                        <div><span><strong>Inside EA</strong></span><strong>25-Aug-2021</strong></div>
                                        <h2>Using our patents to drive positive play</h2>
                                        <p>Announcing a new patent pledge aimed at sharing some of our most innovative
                                            technology.</p>
                                    </section>
                                </article>
                            </li>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/news/www-ea/images/2021/07/eapl21-featured-thumbnail-final.png.adapt.crop16x9.431p.png"
                                        alt="EA Play Live photo">
                                    <section>
                                        <div><span><strong>Electronic Arts
                                                    Inc.</strong></span><strong>22-Jul-2021</strong></div>
                                        <h2>EA Play Live, in Brief</h2>
                                        <p>Everything you need to know about EA Play Live 2021!</p>
                                    </section>
                                </article>
                            </li>
                            <li>
                                <article><img
                                        src="https://media.contentapi.ea.com/content/dam/battlefield/battlefield-2042/images/2021/08/kin-bb-prtl-mintext-1.jpg.adapt.crop16x9.431p.jpg"
                                        alt="Battlefield 2042 Poster">
                                    <section>
                                        <div><span><strong>Electronic Arts
                                                    Inc.</strong></span><strong>22-Jul-2021</strong></div>
                                        <h2>Battlefield Briefing - Exploring Battlefield™ Portal</h2>
                                        <p>Your Battlefield. Your Rules. Everything you need to know about Battlefield™
                                            Portal</p>
                                    </section>
                                </article>
                            </li>
                        </ul>
                        <div class="read-more"><button><a href="#">Read More</a></button></div>
                    </section>
                </section>
                <section class="ea-play">
                    <div><img class="bg-img"
                            src="https://media.contentapi.ea.com/content/dam/eacom/subscription/ea-play-platform-landing-page/images/2021/01/hero-medium-console-7x2-xl.jpg.adapt.crop5x2.1455w.jpg"
                            alt="EA Play Photo">
                        <div class="parental-control">
                            <div>
                                <h2><strong>Parents: Video game control is in your hands.</strong></h2>
                                <div class="learn-more"><button><a href="#">Learn More</a></button></div>
                            </div>
                        </div>
                        <div class="ea-play-info"><img
                                src="https://media.contentapi.ea.com/content/dam/eacom/subscription/ea-play/common/hero-logos/color/ea-play-logo-coral-hero-logo-small.svg">
                            <p>Don’t just get the game. Get more from your game. Unlock exclusive rewards, members-only
                                content, and a library of top titles.</p>
                            <div class="join-now"><button><a href="#">Join Now</a></button></div>
                        </div>
                    </div>
                </section>
            </main>
            <footer>
                <div class="footer1">
                    <div class="container">
                        <ul>
                            <li><a href="#">Impact Report</a></li>
                            <li><a href="#">Executives</a></li>
                            <li><a href="#">Privacy &amp; Security</a></li>
                            <li><a href="#">Political Activities</a></li>
                            <li><a href="#">Global Human Rights Statement</a></li>
                        </ul>
                        <div class="social-handles">
                            <p><strong>Join The Conversation</strong></p>
                            <div class="social-links">
                                <ul>
                                    <li><a href="#"><i class="bi bi-facebook"></i></a></li>
                                    <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                                    <li><a href="#"><i class="bi bi-youtube"></i></a></li>
                                    <li><a href="#"><i class="bi bi-instagram"></i></a></li>
                                    <li><a href="#"><i class="bi bi-discord"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer2">
                    <div class="container"><a href=""><img
                                src="https://media.contentapi.ea.com/content/dam/eacom/en-gb/common/october-ea-ring.png"
                                alt="EA Logo"></a>
                        <ul>
                            <li><a href="#">Game Library</a></li>
                            <li><a href="#">Subscribe</a></li>
                            <li><a href="#">Origin</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Accessibility</a></li>
                            <li><a href="#">Help</a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <div>
        <img src="http://wallpapercave.com/wp/xFrBI00.jpg" style="width: 100%">
    </div>
    <!-- <div class="aboutus">
            <div class="leftaboutus">
                <img src="https://static01.nyt.com/images/2019/04/16/sports/16onsoccerweb-2/merlin_153612873_5bb119b9-8972-4087-b4fd-371cab8c5ba2-superJumbo.jpg">
            </div>
            <div class="rightaboutus">

            </div>
        </div> -->
    <!--Main End-->
    <!--About Us Start-->



    <!--About Us Start-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
    {{-- <script src="{{asset('js/homejs.js')}}"></script> --}}
</body>

</html>
